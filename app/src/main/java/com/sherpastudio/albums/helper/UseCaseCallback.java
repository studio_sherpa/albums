package com.sherpastudio.albums.helper;

public interface UseCaseCallback<R> {
    void onSuccess(R response);
    void onError(Throwable t);
}
