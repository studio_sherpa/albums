package com.sherpastudio.albums.model.repository.albums.remote;

public interface IRetrofitClient {
    AlbumsAPIService getService();
}
