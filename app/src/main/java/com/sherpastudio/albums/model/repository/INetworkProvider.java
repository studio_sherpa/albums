package com.sherpastudio.albums.model.repository;

public interface INetworkProvider {
    boolean hasInternetConnection();
}
